use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct FeedListSettings {
    pub only_show_relevant: bool,
}

impl Default for FeedListSettings {
    fn default() -> Self {
        FeedListSettings {
            only_show_relevant: false,
        }
    }
}
