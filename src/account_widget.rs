use crate::util::GtkUtil;
use gtk4::{prelude::*, subclass::prelude::*, Box, CompositeTemplate, Image, Label, Widget};
use news_flash::models::VectorIcon;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/account_widget.ui")]
    pub struct AccountWidget {
        #[template_child]
        pub logo: TemplateChild<Image>,
        #[template_child]
        pub user_label: TemplateChild<Label>,
    }

    impl Default for AccountWidget {
        fn default() -> Self {
            Self {
                logo: TemplateChild::default(),
                user_label: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AccountWidget {
        const NAME: &'static str = "AccountWidget";
        type Type = super::AccountWidget;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AccountWidget {}

    impl WidgetImpl for AccountWidget {}

    impl BoxImpl for AccountWidget {}
}

glib::wrapper! {
    pub struct AccountWidget(ObjectSubclass<imp::AccountWidget>)
        @extends Widget, Box;
}

impl AccountWidget {
    pub fn new() -> Self {
        glib::Object::new(&[])
    }

    pub fn set_account(&self, vector_icon: Option<VectorIcon>, user_name: &str) {
        let imp = imp::AccountWidget::from_instance(self);

        imp.user_label.set_text(user_name);
        imp.logo.set_from_icon_name(Some("feed-service-generic"));

        let scale = GtkUtil::get_scale(self);
        if let Some(vector_icon) = vector_icon {
            let icon =
                GtkUtil::create_texture_from_bytes(&vector_icon.data, vector_icon.width, vector_icon.height, scale)
                    .expect("Failed to create surface from service icon");
            imp.logo.set_from_paintable(Some(&icon));
        }
    }
}
